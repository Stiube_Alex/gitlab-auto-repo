# Gitlab auto repo

Gitlab auto repo is a small Bash tool that initialize a new repository on GitLab.

## Usage

1. Change `gitlab.public.key` to `gitlab.key` and put you GitLab key in it.


2. From terminal run:

```bash
bash script.sh
```

3. Type the project name.

## Notes

The `data` variable contain the data in the request to the GitLab server, from there the repository configuration can be changed.
