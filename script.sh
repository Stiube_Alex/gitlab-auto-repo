#!/bin/bash

echo "============================== START =============================="

echo "Project name:";
read project_name;
project_url=$(echo $project_name | tr " " "-");
key=$(cat ./gitlab.key);
res_name="log.txt";
echo "start" > $res_name

data="\"name\": \"$project_name\", \"initialize_with_readme\": false"
curl --request POST --header "PRIVATE-TOKEN: $key" \
--header "Content-Type: application/json" --data "{$data}" \
--url 'https://gitlab.com/api/v4/projects/' >> $res_name

mkdir "$project_name"
cd "./$project_name"
echo "generated_folder" >> ../$res_name

git init --initial-branch=main >> ../$res_name
git remote add origin "https://gitlab.com/Stiube_Alex/${project_url}.git" >> ../$res_name

echo "# $project_name <br/>" >readme.md
echo "generated_readme" >> ../$res_name

git add . >> ../$res_name
git commit -m "initial" >> ../$res_name
git push -u origin main >> ../$res_name

echo 'done' >> ../$res_name

echo "============================== DONE =============================="
